const express = require('express');
const passport = require('passport');
const jwtService = require('../services/JwtService');
const router = express.Router();

router.post('/login', passport.authenticate('local', {session:false}), async function (req, res) {
    //Lege nach erfolgreicher ausgeführter LocalStrategy einen JWT-Token an
    const jwtToken = jwtService.signUser(req.user);
    return res.send(jwtToken);
  });

  module.exports = router;