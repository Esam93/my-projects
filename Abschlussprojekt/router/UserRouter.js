const express = require('express');
const passport = require('passport');
const router = express.Router();

router.use(passport.authenticate('jwt', {session: false}));

// Routen werden nur aufgerunfen, sofern der JWT gültig ist
router.get('/', (req, res) => {
    console.log(req.user);
    return res.json(req.user); // Gibt aktuell eingeloggten User zurück!
});

router.post('/', (req, res) => {

});

module.exports = router;
