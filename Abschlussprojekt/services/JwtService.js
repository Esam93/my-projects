const jwt = require('jsonwebtoken');

const JWT_SECRET = 'my-32-character-ultra-secure-and-ultra-long-secret';
const JWT_EXPIRES_IN = '1d';

//Token enthält UserID, Username und Email
function signUser(user) {
    return jwt.sign({
        sub: user.ID,
        name: user.Username,
        email: user.Email
    }, JWT_SECRET, {expiresIn: JWT_EXPIRES_IN});
}

module.exports = {
    signUser,
    JWT_SECRET
}