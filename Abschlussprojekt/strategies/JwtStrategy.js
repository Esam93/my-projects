const JwtStrategy = require('passport-jwt').Strategy;
const JwtExtractor = require('passport-jwt').ExtractJwt;
const JwtService = require('../services/JwtService');

const strategyOptions = {
    //Sende JWT als HTTP-Header mit
    jwtFromRequest: JwtExtractor.fromAuthHeaderAsBearerToken(),
    // Secret zum Entschlüsseln vom Token
    secretOrKey: JwtService.JWT_SECRET
};

const strategy = new JwtStrategy(strategyOptions, (jwt, done) => {
    console.log(jwt)
    const user = {userID: jwt.sub, name: jwt.name, email:jwt.email};
    done(null, user);
});

module.exports = strategy;