const LocalStrategy = require('passport-local').Strategy;
const db = require('../database');

// Vergleiche Username und Passwort aus Frontend mit Datenbank
const strategyOptions = {usernameField: 'Username', passwordField: 'Passwort'};
const strategy = new LocalStrategy(strategyOptions,  async (username, password, done) => {
    try {
        const user = await db.checkUser({username, password});
        done(null, user);
    } catch (err) {
        done(err);
    }
});

module.exports = strategy;
