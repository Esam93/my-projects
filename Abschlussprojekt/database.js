// get the client
const mysql = require('mysql2/promise');
const bcrypt = require('bcryptjs');
 
///////////////  Verbindung mit Datenbank herstellen

let connection; 
  mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'gamelibrary'
}).then(con => {
    connection = con;
    console.warn('Database connected');
}).catch(err => console.error(err));

////////////  Füge User zu Datenbank hinzu

async function addUser(user) {

  //Prüfe ob Email bereits vorhanden, werfe Error wenn vorhanden
  var ssql = "SELECT COUNT(email) AS Result FROM user WHERE email = ?";
  const [mail] = await connection.query(ssql, [user.Email]);
  console.log(mail[0].Result);
  var countMail = mail[0].Result;
  if (countMail>0) {
  console.error("The Email is already in use!");
  throw new Error('Email exists')
  }
  // Hashe Passwort und füge User der Datenbank hinzu
  else {
    const passwordHash = await bcrypt.hash(user.Passwort, 10);
    var sql = "INSERT INTO user (Username, Email, Password) VALUES (?,?,?)";
    const [result] = await connection.query(sql, [user.Username, user.Email, passwordHash]);
    console.log("The User was added to the database")
    return result;
  }
}

///////////////  Login-Userabfrage

async function checkUser(userToCreate) {
  // Prüfe ob eingegebener Username in der Datenbank vorhanden ist
  const [result] = await connection.execute('SELECT * FROM user WHERE Username = ? LIMIT 1', [userToCreate.username]);
    if(!result.length) {
        throw new Error('User not found');
    }
    const user = result[0];
    // Prüfe ob eingebenes Passwort stimmt
    const doPasswordsMatch = await bcrypt.compare(userToCreate.password, user.Password);
    if(!doPasswordsMatch) {
        throw new Error('Passwords do not match');
    }
    delete user.password;
    return user;

}

/////////////  Füge Spiel zu Datenbank hinzu

async function addGame(game, userID) {
  var ssql = 'SELECT * FROM games WHERE PlatformID =? AND UserID=? AND RAWGID=? LIMIT 1';
  const [check] = await connection.query(ssql, [game.platformID, userID, game.rawgID]);
    if (check.length>0) {
      throw new Error('Game is already in your Game Library');
    }
    else {
    var sql = "INSERT INTO games (PlatformID, UserID, Name, Releasedate, Genres, Screenshot, RAWGID) VALUES (?,?,?,?,?,?,?)";
    const [result] = await connection.query(sql, [game.platformID, userID, game.name, game.releaseDate, game.genre, game.screenshot, game.rawgID]);
    console.log("The Game was added to the database")
    return result;
    }
  }


/////////////  Füge Spiel zu Datenbank hinzu - Wishlist 

async function addGametoWishlist(game, userID) {
  var ssql = "SELECT * FROM games WHERE UserID=? AND RAWGID=? AND Wishlist = 'True' LIMIT 1";
  const [checkWish] = await connection.query(ssql, [userID, game.rawgID]);
    if (checkWish.length>0) {
      throw new Error('Game is already in your Wishlist');
    }
    else {
    var sql = "INSERT INTO games (UserID, Name, Releasedate, Genres, Screenshot, RAWGID, Wishlist) VALUES (?,?,?,?,?,?,'True')";
    const [result] = await connection.query(sql, [userID, game.name, game.releaseDate, game.genre, game.screenshot, game.rawgID]);
    console.log("The Game was added to the database")
    return result;
    }
  }

///////////// Lösche Spiel aus Datenbank

async function deleteGame(gameData, userID) {
  var sql = "DELETE from games where RAWGID = ? AND UserID= ? AND PlatformID = ?";
  const [result] = await connection.query(sql, [gameData.deleteID, userID, gameData.platformID]);
  console.log("The Game was deleted");
  return result;
}

///////////// Lösche Spiel aus Datenbank - Wishlist

async function deleteGameFromWishlist(gameData, userID) {
  var sql = "DELETE from games where RAWGID = ? AND UserID= ? AND Wishlist = 'True'";
  const [result] = await connection.query(sql, [gameData.deleteID, userID]);
  console.log("The Game was deleted");
  return result;
}

///////////// Lösche Spiel aus Datenbank - All Games

async function deleteGameFromAll(gameData, userID) {
  var sql = "DELETE from games where RAWGID = ? AND UserID= ? AND PlatformID = (SELECT PlatformID FROM platform WHERE Name = ?)";
  const [result] = await connection.query(sql, [gameData.deleteID, userID, gameData.deletePlatName]);
  console.log("The Game was deleted");
  return result;
}

/////////////  Platform-Abfrage

async function showPlatforms() {

  var sql = "SELECT PlatformID, name, image, link from Platform";
  const [platforms] = await connection.query(sql);
  console.log(platforms);
  return platforms;
}

////////////// Eigene Spieleausgabe

async function showGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.platformID = p.platformID ORDER BY g.name"; 
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Eigene Spieleausgabe - Wishlist

async function showGamesWishlist(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID from Games g WHERE g.UserID = ? AND Wishlist='True' ORDER BY g.name"; 
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PC

async function showPCGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 1 AND p.platformID = 1 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation

async function showPSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 2 AND p.platformID = 2 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation 2

async function showPS2Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 3 AND p.platformID = 3 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation 3

async function showPS3Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 4 AND p.platformID = 4 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation 4

async function showPS4Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 5 AND p.platformID = 5 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation 5

async function showPS5Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 6 AND p.platformID = 6 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation Portable

async function showPSPGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 7 AND p.platformID = 7 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// PlayStation Vita

async function showPSVitaGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 8 AND p.platformID = 8 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Xbox

async function showXboxGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 9 AND p.platformID = 9 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Xbox 360

async function showXbox360Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 10 AND p.platformID = 10 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Xbox One

async function showXboxOneGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 11 AND p.platformID = 11 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Xbox Series X/S

async function showXboxSeriesXSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 12 AND p.platformID = 12 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// NES

async function showNESGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 13 AND p.platformID = 13 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// SNES

async function showSNESGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 14 AND p.platformID = 14 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo 64

async function showN64Games(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 15 AND p.platformID = 15 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo Gamecube

async function showGCGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 16 AND p.platformID = 16 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo Wii

async function showWiiGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 17 AND p.platformID = 17 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo Wii U

async function showWiiUGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 18 AND p.platformID = 18 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo Switch

async function showSwitchGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 19 AND p.platformID = 19 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Gameboy

async function showGameboyGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 20 AND p.platformID = 20 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Gameboy Color

async function showGBCGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 21 AND p.platformID = 21 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Gameboy Advance

async function showGBAGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 22 AND p.platformID = 22 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo DS

async function showDSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 23 AND p.platformID = 23 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Nintendo 3DS

async function show3DSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 24 AND p.platformID = 24 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Sega Master System

async function showSegaMSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 25 AND p.platformID = 25 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Sega Genesis/Mega Drive

async function showSegaGMGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 26 AND p.platformID = 26 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Sega Saturn

async function showSegaSGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 27 AND p.platformID = 27 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

////////////// Sega Dreamcast

async function showSegaDGames(body, userID) {
  var sql = "SELECT g.name, g.screenshot, g.RAWGID, p.name AS 'Platform' from Games g, platform p WHERE g.UserID = ? AND g.PlatformID = 28 AND p.platformID = 28 ORDER BY g.name";
  const [games] = await connection.query(sql, [userID]);
  console.log(games);
  return games;
}

module.exports = {
  addUser,
  checkUser,
  addGame,
  addGametoWishlist,
  deleteGame,
  deleteGameFromWishlist,
  deleteGameFromAll,
  showPlatforms,
  showGames,
  showGamesWishlist,
  showPCGames,
  showPSGames,
  showPS2Games,
  showPS3Games,
  showPS4Games,
  showPS5Games,
  showPSPGames,
  showPSVitaGames,
  showXboxGames,
  showXbox360Games,
  showXboxOneGames,
  showXboxSeriesXSGames,
  showNESGames,
  showSNESGames,
  showN64Games,
  showGCGames,
  showWiiGames,
  showWiiUGames,
  showSwitchGames,
  showGameboyGames,
  showGBCGames,
  showGBAGames,
  showDSGames,
  show3DSGames,
  showSegaMSGames,
  showSegaGMGames,
  showSegaSGames,
  showSegaDGames
}
