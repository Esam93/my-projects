const express = require('express');
const app = express();
const UserRouter = require('./router/UserRouter');
const PublicRouter = require('./router/PublicRouter');
const bodyParser = require('body-parser');
const db = require('./database');
const path = require('path');
const https = require('https');
const passport = require('passport');
const LocalStrategy = require('./strategies/LocalStrategy');
const JwtStrategy = require('./strategies/JwtStrategy');
const axios = require('axios');

const port = 3000


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));

/////////////////////  Passport wird initialisiert, und Local Strategy und JWT Strategy eingebunden

app.use(passport.initialize());
passport.use('local', LocalStrategy);
passport.use('jwt', JwtStrategy);

////////////////////  Setzte User und Public-Router

app.use('/api/user', UserRouter);
app.use('/api', PublicRouter);


app.listen(port, () => {
    console.log(`Game Library app listening at http://localhost:${port}`)
  });

/////////////////////  Sende Daten aus Registrierung an die Datenbank und prüfe auf Konflikt 

app.post('/landing-page', async function (req, res){
  console.log (req.body);
  let user, success = true;
  try {
    user = await db.addUser(req.body);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, user});
});


///////////////////  Sende Spiele aus Suchergebnisse an Datenbank

app.post('/addGame', passport.authenticate('jwt',  {session: false}), async function (req, res) {
  console.log (req.body);
  console.log(req.user);
  console.log("Test Nr.1");
  let game, success = true;
  try {
    game = await db.addGame(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, game});
 }); 

 ///////////////////  Sende Spiele aus Suchanfrage an Datenbank - Wishlist

app.post('/addGametoWishlist', passport.authenticate('jwt',  {session: false}), async function (req, res) {
  console.log (req.body);
  console.log(req.user);
  console.log("Test Wishlist");
  let game, success = true;
  try {
    game = await db.addGametoWishlist(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, game});
 }); 

//////////////////// Lösche Spiel aus Datenbank

app.post('/delete', passport.authenticate('jwt',  {session: false}), async function (req, res) {
  console.log (req.body);
  console.log(req.user);
  console.log("Test Nr.2")
  let dlgame, success = true;
  try {
    dlgame = await db.deleteGame(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, dlgame});
 }); 

 //////////////////// Lösche Spiel aus Datenbank

app.post('/deleteGameFromWishlist', passport.authenticate('jwt',  {session: false}), async function (req, res) {
  console.log (req.body);
  console.log(req.user);
  console.log("Test Nr.2")
  let dlgame, success = true;
  try {
    dlgamewl = await db.deleteGameFromWishlist(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, dlgame});
 }); 

  //////////////////// Lösche Spiel aus All Games

app.post('/deleteGameFromAll', passport.authenticate('jwt',  {session: false}), async function (req, res) {
  console.log (req.body);
  console.log(req.user);
  console.log("Test Nr.2")
  let dlgame, success = true;
  try {
    dlgameall = await db.deleteGameFromAll(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
    res.status(409)
  };
  res.json({success, dlgame});
 }); 

////////////////////  Platformausgabe

app.get('/platformsJSON', async function (req, res){
  console.log (req.body);
  let platform, success = true;
  try {
    platform = await db.showPlatforms(req.body);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, platform});
});

////////////////  Spieleausgabe 

app.get('/gamesJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.3");
  let games, success = true;
  try {
    games = await db.showGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Spieleausgabe - Wishlist

app.get('/wishlistJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.3");
  let games, success = true;
  try {
    games = await db.showGamesWishlist(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PC

app.get('/pcJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.4");
  let games, success = true;
  try {
    games = await db.showPCGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation

app.get('/psJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.5");
  let games, success = true;
  try {
    games = await db.showPSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation 2

app.get('/ps2JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.6");
  let games, success = true;
  try {
    games = await db.showPS2Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation 3

app.get('/ps3JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.7");
  let games, success = true;
  try {
    games = await db.showPS3Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation 4

app.get('/ps4JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.8");
  let games, success = true;
  try {
    games = await db.showPS4Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation 5

app.get('/ps5JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.9");
  let games, success = true;
  try {
    games = await db.showPS5Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation Portable

app.get('/pspJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.10");
  let games, success = true;
  try {
    games = await db.showPSPGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  PlayStation Vita

app.get('/psvitaJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.11");
  let games, success = true;
  try {
    games = await db.showPSVitaGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Xbox

app.get('/xboxJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.12");
  let games, success = true;
  try {
    games = await db.showXboxGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Xbox 360

app.get('/xbox360JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.13");
  let games, success = true;
  try {
    games = await db.showXbox360Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Xbox One

app.get('/xboxoneJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.14");
  let games, success = true;
  try {
    games = await db.showXboxOneGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Xbox Series X/S

app.get('/xboxseriesxsJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.15");
  let games, success = true;
  try {
    games = await db.showXboxSeriesXSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  NES

app.get('/nesJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.16");
  let games, success = true;
  try {
    games = await db.showNESGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  SNES

app.get('/snesJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.17");
  let games, success = true;
  try {
    games = await db.showSNESGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo 64

app.get('/n64JSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.17");
  let games, success = true;
  try {
    games = await db.showN64Games(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo Gamecube

app.get('/gcJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.18");
  let games, success = true;
  try {
    games = await db.showGCGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo Wii

app.get('/wiiJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.19");
  let games, success = true;
  try {
    games = await db.showWiiGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo Wii U

app.get('/wiiuJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.20");
  let games, success = true;
  try {
    games = await db.showWiiUGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo Switch

app.get('/switchJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.21");
  let games, success = true;
  try {
    games = await db.showSwitchGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Gameboy

app.get('/gameboyJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.22");
  let games, success = true;
  try {
    games = await db.showGameboyGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Gameboy Color

app.get('/gbcJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.23");
  let games, success = true;
  try {
    games = await db.showGBCGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Gameboy Advance

app.get('/gbaJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.23");
  let games, success = true;
  try {
    games = await db.showGBAGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo DS

app.get('/dsJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.24");
  let games, success = true;
  try {
    games = await db.showDSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Nintendo 3DS

app.get('/3dsJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.25");
  let games, success = true;
  try {
    games = await db.show3DSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Sega Master System

app.get('/segamsJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.26");
  let games, success = true;
  try {
    games = await db.showSegaMSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Sega Genesis/Mega Drive

app.get('/segagmJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.27");
  let games, success = true;
  try {
    games = await db.showSegaGMGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Sega Saturn

app.get('/segasJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.28");
  let games, success = true;
  try {
    games = await db.showSegaSGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});

////////////////  Sega Dreamcast

app.get('/segadJSON', passport.authenticate('jwt',  {session: false}), async function (req, res){
  console.log (req.user);
  console.log ("Test Nr.29");
  let games, success = true;
  try {
    games = await db.showSegaDGames(req.body,req.user.userID);
  }
  catch (error){
    console.log(error)
    success=false
  };
  res.json({success, games});
});