This project was developed with HTML, CSS, JavaScript, jQuery and Vue.js in the Frontend, Node.js in the Backend and SQL for data processing. 
Games and associated Data are provided by RAWG - Database.
For further information please review the documentation or watch the video to get a glimpse of the project.
Important Notice: Due to security reasons the API-Key to connect to the RAWG - Database was removed from the Code.