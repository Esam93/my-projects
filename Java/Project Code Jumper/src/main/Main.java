package main;

import java.util.logging.Level;
import java.util.logging.Logger;

import gamePackage.Game;

/***************************************************************************************
* 	@Authors: Esam Ahmed & Martin Eidler
* 	Date: 05-08-2021
***************************************************************************************/

public class Main {

	public static void main(String[] args) {
		
		Game game = new Game(1940, 1080);
        game.newGame();


	}

}
