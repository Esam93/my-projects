package fahrzeuge;



public class LKW extends Fahrzeug {
	
	
	//--------------------------------------------Constructor-----------------------------
	
	public LKW(int id, int baujahr, double grundpreis, String marke) {
		super(id, baujahr, grundpreis, marke);
	}


	//-----------------------------------------------Methoden--------------------------------------
	
	@Override
	public void print() {
		
		System.out.print("Id: " + super.getId() + "\n"
		+ "Baujahr: "+ super.getBaujahr() + "\n"
		+ "Marke: " + super.getMarke() + "\n" 
		+ "Grundpreis: " + super.getGrundpreis() + "\n"
		+ "Rabatt in Prozent: " + getRabatt() + "\n"
		+ "Der finale Preis betr�gt: " + getFinalerPreis()+ "\n");
	}


	@Override
	public double getFinalerPreis() {
		double finalerPreisLKW = super.getGrundpreis() - (super.getGrundpreis()/100*getRabatt());
		return finalerPreisLKW;
	}


	@Override
	public double getRabatt() {
		int aktuellesJahr = 2021;
		double rabatt = (aktuellesJahr - super.getBaujahr())*6;
		if (rabatt>15) {
			rabatt=15;
		}
		
		return rabatt;
	}
}

