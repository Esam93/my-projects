package fahrzeuge;

public class PKW extends Fahrzeug{


	private int endeVorfuehrjahr=2000;	
	private boolean jahrKontrolle=true;
	
	//--------------------------------------------Constructor-----------------------------

	
	public PKW(int id, int baujahr, double grundpreis, String marke, int endeVorfuehrJahr, boolean jahrKontrolle) {
		super(id, baujahr, grundpreis, marke);
		this.endeVorfuehrjahr = endeVorfuehrJahr;
		this.jahrKontrolle = jahrKontrolle;
		
	}

	

	//--------------------------------------------Getter & Setter-----------------------------
	


	public int getEndeVorfuehrjahr() {
		return endeVorfuehrjahr;
	}



	public void setEndeVorfuehrjahr(int endeVorfuehrjahr) {
		this.endeVorfuehrjahr = endeVorfuehrjahr;
	}



	public boolean isJahrKontrolle() {
		return jahrKontrolle;
	}



	public void setJahrKontrolle(boolean jahrKontrolle) {
		this.jahrKontrolle = jahrKontrolle;
	}


	
	//-----------------------------------------------Methoden--------------------------------------


	@Override
	public double getRabatt() {
	

		int aktuellesJahr = 2021;

		double rabatt = ((aktuellesJahr - super.getBaujahr())*5)+((aktuellesJahr- super.getBaujahr())*3);
		
		if (rabatt>=10) {
			rabatt=10;
		}
			
		return rabatt;
	}
	
	@Override
	public double getFinalerPreis() {
		
		double finalerPreisPKW = super.getGrundpreis() - (super.getBaujahr()/100*getRabatt());
		return finalerPreisPKW;
	}
	
	public boolean kontrolleVorfuehrjahr() {
		int aktuellesJahr = 2021;
		if (endeVorfuehrjahr>aktuellesJahr || endeVorfuehrjahr<super.getBaujahr()) {
			jahrKontrolle = false;
			System.err.println("Die Eingabe ist ung�ltig");
		} 
		else {
			jahrKontrolle=true;
		}	
		return jahrKontrolle;
	}
	
	
	public void print() {
	
		System.out.print("Id: " + super.getId() + "\n"
		+ "Baujahr: "+ super.getBaujahr() + "\n"
		+ "Marke: " + super.getMarke() + "\n" 
		+ "Grundpreis: " + super.getGrundpreis() + "\n"
		+ "Rabatt in Prozent: " + getRabatt() + "\n"
		+ "Der finale Preis betr�gt: " + getFinalerPreis() + "\n");
	}

	public boolean setEndeVorfuehrjahr() {
		int aktuellesJahr = 2021;
		if (endeVorfuehrjahr>aktuellesJahr || endeVorfuehrjahr<super.getBaujahr()) {
			jahrKontrolle = false;
			System.err.println("Die Eingabe ist ung�ltig");
		} 
		else {
			jahrKontrolle=true;
		}	
		return jahrKontrolle;
	}


	
	
}
