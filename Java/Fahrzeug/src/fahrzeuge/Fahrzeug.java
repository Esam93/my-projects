package fahrzeuge;

public abstract class Fahrzeug {

	private int id;
	private int baujahr;
	private double grundpreis;
	private String marke;
	
	//Constructor
	
	public Fahrzeug(int id, int baujahr, double grundpreis, String marke) {
		super();
		this.id = id;
		this.baujahr = baujahr;
		this.grundpreis = grundpreis;
		this.marke = marke;
	}
	
	//Methoden
	
	public abstract double getRabatt();
	public abstract double getFinalerPreis();
	public abstract void print();

	//Setter & Getter
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public double getGrundpreis() {
		return grundpreis;
	}

	public void setGrundpreis(double grundpreis) {
		this.grundpreis = grundpreis;
	}

	public String getMarke() {
		return marke;
	}

	public void setMarke(String marke) {
		this.marke = marke;
	}
	
	
	
	
	
}
