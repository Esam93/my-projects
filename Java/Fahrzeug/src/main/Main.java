package main;


import autohaus.Autohaus;
import fahrzeuge.Fahrzeug;
import fahrzeuge.LKW;
import fahrzeuge.PKW;

public class Main {

	
	public static void main(String[] args) {
	 
	
			Autohaus autohaus = new Autohaus(10, false, "Cid's Autohaus");
			autohaus.setFahrzeug("PKW", 1, "Audi", 2000, 5000, 2021, true);
			autohaus.setFahrzeug("PKW", 2, "Mercedes", 2012, 15000, 2021, true);
			autohaus.setFahrzeug("PKW", 3, "VW", 2006, 7500, 2021, true);
			autohaus.setFahrzeug("LKW", 1, "Volvo", 2008, 9000, 2021, true);
			autohaus.setFahrzeug("LKW", 2, "VW", 1998, 4500, 2021, true);
	
			autohaus.print();
					
	
			for (int i = 1; i <= autohaus.getListeFahrzeuge().size(); i++) {
				System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: " + i);
				
				while (i<autohaus.getListeFahrzeuge().size()) {
					System.out.println("Es sind noch nicht alle Fahrzeuge registriert");
					break;
				}
				if (i==autohaus.getListeFahrzeuge().size()) {
					System.out.print("Folgende Marken sind zu verkaufen: ");
					for (int j = 0; j < autohaus.getListeFahrzeuge().size()-1; j++) {
						System.out.print(autohaus.getListeFahrzeuge().get(j).getMarke() + ", ");
					}
					System.out.println(autohaus.getListeFahrzeuge().get(i-1).getMarke());
					
					System.out.println("Folgende Marken sind zu verkaufen: ");
					for (Fahrzeug fahrzeug : autohaus.getListeFahrzeuge()) {
						System.out.println(fahrzeug.getMarke());
					}
				}
			}
			
			System.out.println("Das Autohaus ist wegen fehlender Mitarbeiter*innen überlastet: " + autohaus.kontrolleAutohaus(5));
			System.out.println("Das Autohaus ist wegen fehlender Abstellplätze überlastet: " + autohaus.kontrolleAutohaus());
			
			for (int i = 0; i < autohaus.getListeFahrzeuge().size(); i++) {
				System.out.println("Der Preis von Auto Nr." + (i+1) + " beträgt: " + autohaus.getListeFahrzeuge().get(i).getFinalerPreis());
			}
			
			
			for (int i = 0; i < autohaus.getListeFahrzeuge().size(); i++) {
				if (autohaus.getListeFahrzeuge().get(i) instanceof LKW) {
					System.out.println("Das Fahrzeug Nr." + (i+1) + " ist ein LKW");
				}
				if (autohaus.getListeFahrzeuge().get(i) instanceof PKW) {
					System.out.println("Das Fahrzeug Nr." + (i+1) + " ist ein PKW");
				}
			}
			
			
	}
	
}
	
		

