package autohaus;

import java.util.LinkedList;
import java.util.List;

import fahrzeuge.Fahrzeug;
import fahrzeuge.LKW;
import fahrzeuge.PKW;

public class Autohaus {

	
	int abstellplätze=5;
	boolean zuVieleFahrzeuge=false;
	String autohausName = "Cid's Autohaus";
	
	List<Fahrzeug> listeFahrzeuge = new LinkedList<Fahrzeug>();

	
	//--------------------------------------------Constructor-----------------------------
	
	 
	
	public Autohaus(int abstellplätze, boolean zuVieleFahrzeuge, String autohausName) {
		super();
		this.abstellplätze = abstellplätze;
		this.zuVieleFahrzeuge = zuVieleFahrzeuge;
		this.autohausName = autohausName;
		this.listeFahrzeuge = listeFahrzeuge;
	}
	
	
	//--------------------------------------------Getter & Setter-----------------------------
	
	
	
	public int getAbstellplätze() {
		return abstellplätze;
	}


	public void setAbstellplätze(int abstellplätze) {
		this.abstellplätze = abstellplätze;
	}


	public boolean isZuVieleFahrzeuge() {
		return zuVieleFahrzeuge;
	}


	public void setZuVieleFahrzeuge(boolean zuVieleFahrzeuge) {
		this.zuVieleFahrzeuge = zuVieleFahrzeuge;
	}


	public String getAutohausName() {
		return autohausName;
	}


	public void setAutohausName(String autohausName) {
		this.autohausName = autohausName;
	}


	public List<Fahrzeug> getListeFahrzeuge() {
		return listeFahrzeuge;
	}


	public void setListeFahrzeuge(List<Fahrzeug> listeFahrzeuge) {
		this.listeFahrzeuge = listeFahrzeuge;
	}
	
	//-----------------------------------------------Methoden--------------------------------------

	public boolean kontrolleAutohaus() {
		if (listeFahrzeuge.size()>abstellplätze) {
			zuVieleFahrzeuge=true;
		}
		else {
			zuVieleFahrzeuge=false;
		}
		
		return zuVieleFahrzeuge;
	}
	
	// Beim Aufrufen der Methode kontrolleAutohaus() unterscheidet Eclipse zwischen dem oberen und dem oberen (gleicher Name!!) durch den Parameter. 
	// Will ich das untere aufrufen dann Parameter mitgeben. Will ich das obere aufrufen, dann ohne Parameter.



	public boolean kontrolleAutohaus(int anzahlMitarbeiterInnen) {
		if (anzahlMitarbeiterInnen*3>listeFahrzeuge.size()) {
			zuVieleFahrzeuge=false;
		}
		else {
			zuVieleFahrzeuge=true;
		}
		return zuVieleFahrzeuge;
	}




	public void print() {
		System.out.println("~~~<"+autohausName+">~~~~");
	}
	
	public void setFahrzeug (String fahrzeugTyp, int id, String marke, int baujahr, double grundpreis, int endeVorfuehrjahr, boolean jahrKontrolle) {
		if (fahrzeugTyp == "LKW") {
			LKW lkw = new LKW(id, baujahr, grundpreis, marke);
			listeFahrzeuge.add(lkw);
		}
		else if (fahrzeugTyp == "PKW") {
			PKW pkw = new PKW(id, baujahr, grundpreis, marke, endeVorfuehrjahr, jahrKontrolle);
			listeFahrzeuge.add(pkw);
		}
		else {
			System.out.println("Der Fahrzeugtyp ist uns nicht bekannt");
		}
	}
	
	
}
