// get the client
const mysql = require('mysql2/promise');
 
// Verbindung mit Datenbank herstellen
let connection; 
  mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'kontakt'
}).then(con => {
    connection = con;
    console.log('Database connected');
}).catch(err => console.error(err));

// Inhalte aus Nachricht in Datenbank einfügen

async function addMessage(message) {
    var sql = "INSERT INTO messages (Vorname, Nachname, Email, Nachricht) VALUES (?,?,?,?)";
    const [result] = await connection.query(sql, [message.Vorname, message.Nachname,message.Email,message.Nachricht]);
    return result;
}

module.exports = {
    addMessage
}