
// Sidenav

function openNav() {
    document.getElementById("mySidenav").style.width = "350px";
    document.getElementById("main").style.marginLeft = "350px";
    }
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    }

// Infobox

$(document).ready(function(){
 $('[data-toggle="popover"]').popover();
});

//Dropdown

function openDrop() {
  document.getElementById("myDropdown").classList.toggle("show");
}


//Font-Size


function increaseFontSize() {
  var max = 24;
  var i;
  var p = document.getElementsByTagName('p');
  for (i = 0; i < p.length; i++) {
    //Condition ist true, falls Size gesetzt ist
    if (p[i].style.fontSize) {
      //Hole Font-Size und replace px damit es zu int wird
      var s = parseInt(p[i].style.fontSize.replace("px", ""));
    } else {
    //Wenn keine Size gesetzt dann auf 16
      var s = 16;
    } 
    if (s != max) {
      s += 1;
    }
    //Berechnete Zahl wird mit px eingefügt
    p[i].style.fontSize = s + "px"
  }
}

function decreaseFontSize() {
  var min = 8;
  var i;
  var p = document.getElementsByTagName('p');
  for (i = 0; i < p.length; i++) {
    if (p[i].style.fontSize) {
      var s = parseInt(p[i].style.fontSize.replace("px", ""));
    } else {
      var s = 16;
    }
     if (s != min) {
      s -= 1;
    }
    p[i].style.fontSize = s + "px"
  }
}