const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000
const db = require('./database')
const nodemailer = require("nodemailer");
require('dotenv').config();

const path = require('path');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/BA-00-Startseite.html'));
});

// Daten aus Kontakt-Form speichern

app.post('/contact', async function (req, res){
  console.log (req.body);
  try {
    await db.addMessage(req.body);
    await sendMail(req.body);
  }
  catch (error){console.log(error)};
  res.json({success: true});
});

// Email Verbindung aufbauen

async function sendMail(contact_form) {

  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: process.env.DB_USER, // Abgesichert durch env
      pass: process.env.DB_PASS, // Abgesichert durch env
    },
  });

  // Email mit Inhalte aus Kontakt-Form versenden

  let vorname=JSON.stringify(contact_form.Vorname).replace(/"/g,"");
  let nachname=JSON.stringify(contact_form.Nachname).replace(/"/g,""); 
  let info = await transporter.sendMail({
    from: contact_form.Email, // sender address
    to: "shady313x@gmail.com", // list of receivers
    subject: "Neue Nachricht von " + vorname + " " + nachname, // Subject line
    //text: JSON.stringify(contact_form), // plain text body
    html: "<p>" + JSON.stringify(contact_form.Nachricht) + "</p> <br> <p>Gesendet von</p> " + JSON.stringify(contact_form.Email), // html body
  });

  console.log("Die Nachricht wurde erfolgreich versendet", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

}


//Download einrichten
app.get('/downlaod', (res,req) =>{
  res.download(path.join(__dirname + '/Ahmed, Esam. Videospiele als (politik-)wissenschaftliche Herausforderung. Eine Falluntersuchng des Spiels The Last of Us im Lichte geschlechtertheoretischer Überlegungen. Bachelorarbeit. Universität Wien. 2017.pfd'), 'Ahmed, Esam. Videospiele als (politik-)wissenschaftliche Herausforderung. Eine Falluntersuchng des Spiels The Last of Us im Lichte geschlechtertheoretischer Überlegungen. Bachelorarbeit. Universität Wien. 2017.pdf' );
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});